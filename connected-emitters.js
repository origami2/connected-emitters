var shortid = require('shortid');
var debug = require('debug')('origami:ConnectedEmitters');

function invoke(fn, self, args) {
  switch (args.length) {
    case 0:
      return fn.call(self);
    case 1:
      return fn.call(self, args[0]);
    case 2:
      return fn.call(self, args[0], args[1]);
    case 3:
      return fn.call(self, args[0], args[1], args[2]);
    case 4:
      return fn.call(self, args[0], args[1], args[2], args[3]);
    default:
      return fn.apply(self, args);
  }
}

function invokeSkipOne(fn, self, args) {
  switch (args.length) {
    case 1:
      return fn.call(self, args[1]);
    case 2:
      return fn.call(self, args[1], args[2]);
    case 3:
      return fn.call(self, args[1], args[2], args[3]);
    case 4:
      return fn.call(self, args[1], args[2], args[3], args[4]);
    default:
      var a = [];
      
      for (var i = 1; i < args.length; i++) {
        a.push(args[i]);
      }
    
      return fn.apply(self, a);
  }
}

function FakeEmitter(anyEvent, id) {
  this.anyEvent = anyEvent;
  this.handlers = {
    '': []
  };
  this.id = id;
}

FakeEmitter.prototype.removeListener = function (eventName,listener) {
  if (!this.handlers[eventName]) return;
  
  this.handlers[eventName].splice(this.handlers[eventName].indexOf(listener), 1);
};

FakeEmitter.prototype.removeAllListeners = function (eventName) {
  delete this.handlers[eventName];
};

FakeEmitter.prototype.onAny = function(listener) {
  debug('%s adding listener for any function', this.id);
  
  this.handlers[''].push(listener);
};

FakeEmitter.prototype.offAny = function(listener) {
  debug('%s removing listener for any function', this.id);
  
  var handlers = this.handlers[''];
  
  if (!handlers) return;
  
  for (var i = handlers.length; i >= 0; i--) {
    if (handlers[i] === listener) handlers.splice(i, 1);
  }
};

FakeEmitter.prototype.fireEvent = function (eventName) {
  debug('%s firing event %s', this.id, eventName);
  
  var handlers = this.handlers[eventName] || [];

  for (var i = 0; i < handlers.length; i++) {
    invokeSkipOne(handlers[i], this, arguments);
  }
  
  for (var i = 0; i < this.handlers[''].length; i++) {
    invoke(this.handlers[''][i], this, arguments);
  }
};

FakeEmitter.prototype.on = function (eventName, listener) {
  debug('%s adding listener for %s', this.id, eventName);
  
  (this.handlers[eventName] = this.handlers[eventName] || []).push(listener);
};

FakeEmitter.prototype.addEventListener = FakeEmitter.prototype.on;

FakeEmitter.prototype.off = function (eventName, listener) {
  debug('%s removing listener for %s', this.id, eventName);
  
  var handlers = this.handlers[eventName];
  
  if (!handlers) return;
  
  for (var i = handlers.length; i >= 0; i--) {
    if (handlers[i] === listener) handlers.splice(i, 1);
  }
};

FakeEmitter.prototype.emit = function () {
  debug('%s emitting %s', this.id, arguments[0]);
    
  invoke(this.anyEvent, this, arguments);
};

function ConnectedEmitters(id) {
  this.id = id || shortid.generate();
  this.emitters = [];
}

ConnectedEmitters.prototype.createEmitter = function () {
  var emitters = this.emitters;
  
  var emitter = new FakeEmitter(
    function () {
      for (var i = 0; i < emitters.length; i++) {
        if (emitters[i] !== emitter) {
          invoke(emitters[i].fireEvent, emitters[i], arguments);
        }
      }
    },
    this.id + ':' + this.emitters.length
  );
  
  emitters
  .push(emitter);
  
  return emitter;
};

module.exports = ConnectedEmitters;